// Generated from HKDataFormatParser.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';

// This class defines a complete listener for a parse tree produced by HKDataFormatParser.
export default class HKDataFormatParserListener extends antlr4.tree.ParseTreeListener {

	// Enter a parse tree produced by HKDataFormatParser#hk.
	enterHk(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#hk.
	exitHk(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#lines.
	enterLines(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#lines.
	exitLines(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#handleLine.
	enterHandleLine(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#handleLine.
	exitHandleLine(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#identifier.
	enterIdentifier(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#identifier.
	exitIdentifier(ctx) {
	}



}