// Generated from HKDataFormatLexer.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';



const serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786",
    "\u5964\u0002\u0007\"\b\u0001\u0004\u0002\t\u0002\u0004\u0003\t\u0003",
    "\u0004\u0004\t\u0004\u0004\u0005\t\u0005\u0004\u0006\t\u0006\u0003\u0002",
    "\u0006\u0002\u000f\n\u0002\r\u0002\u000e\u0002\u0010\u0003\u0002\u0003",
    "\u0002\u0003\u0003\u0005\u0003\u0016\n\u0003\u0003\u0003\u0003\u0003",
    "\u0003\u0004\u0003\u0004\u0003\u0005\u0003\u0005\u0003\u0006\u0006\u0006",
    "\u001f\n\u0006\r\u0006\u000e\u0006 \u0002\u0002\u0007\u0003\u0003\u0005",
    "\u0004\u0007\u0005\t\u0006\u000b\u0007\u0003\u0002\u0004\u0004\u0002",
    "\u000b\u000b\"\"\r\u0002\"\"%%*+0<>>@@B\\^^aac|\u0080\u0080\u0002$\u0002",
    "\u0003\u0003\u0002\u0002\u0002\u0002\u0005\u0003\u0002\u0002\u0002\u0002",
    "\u0007\u0003\u0002\u0002\u0002\u0002\t\u0003\u0002\u0002\u0002\u0002",
    "\u000b\u0003\u0002\u0002\u0002\u0003\u000e\u0003\u0002\u0002\u0002\u0005",
    "\u0015\u0003\u0002\u0002\u0002\u0007\u0019\u0003\u0002\u0002\u0002\t",
    "\u001b\u0003\u0002\u0002\u0002\u000b\u001e\u0003\u0002\u0002\u0002\r",
    "\u000f\t\u0002\u0002\u0002\u000e\r\u0003\u0002\u0002\u0002\u000f\u0010",
    "\u0003\u0002\u0002\u0002\u0010\u000e\u0003\u0002\u0002\u0002\u0010\u0011",
    "\u0003\u0002\u0002\u0002\u0011\u0012\u0003\u0002\u0002\u0002\u0012\u0013",
    "\b\u0002\u0002\u0002\u0013\u0004\u0003\u0002\u0002\u0002\u0014\u0016",
    "\u0007\u000f\u0002\u0002\u0015\u0014\u0003\u0002\u0002\u0002\u0015\u0016",
    "\u0003\u0002\u0002\u0002\u0016\u0017\u0003\u0002\u0002\u0002\u0017\u0018",
    "\u0007\f\u0002\u0002\u0018\u0006\u0003\u0002\u0002\u0002\u0019\u001a",
    "\u0007/\u0002\u0002\u001a\b\u0003\u0002\u0002\u0002\u001b\u001c\u0007",
    "?\u0002\u0002\u001c\n\u0003\u0002\u0002\u0002\u001d\u001f\t\u0003\u0002",
    "\u0002\u001e\u001d\u0003\u0002\u0002\u0002\u001f \u0003\u0002\u0002",
    "\u0002 \u001e\u0003\u0002\u0002\u0002 !\u0003\u0002\u0002\u0002!\f\u0003",
    "\u0002\u0002\u0002\u0006\u0002\u0010\u0015 \u0003\u0002\u0003\u0002"].join("");


const atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

const decisionsToDFA = atn.decisionToState.map( (ds, index) => new antlr4.dfa.DFA(ds, index) );

export default class HKDataFormatLexer extends antlr4.Lexer {

    static grammarFileName = "HKDataFormatLexer.g4";
    static channelNames = [ "DEFAULT_TOKEN_CHANNEL", "HIDDEN" ];
	static modeNames = [ "DEFAULT_MODE" ];
	static literalNames = [ null, null, null, "'-'", "'='" ];
	static symbolicNames = [ null, "WS", "NL", "DASH", "EQUAL", "IDENTIFIER" ];
	static ruleNames = [ "WS", "NL", "DASH", "EQUAL", "IDENTIFIER" ];

    constructor(input) {
        super(input)
        this._interp = new antlr4.atn.LexerATNSimulator(this, atn, decisionsToDFA, new antlr4.PredictionContextCache());
    }

    get atn() {
        return atn;
    }
}

HKDataFormatLexer.EOF = antlr4.Token.EOF;
HKDataFormatLexer.WS = 1;
HKDataFormatLexer.NL = 2;
HKDataFormatLexer.DASH = 3;
HKDataFormatLexer.EQUAL = 4;
HKDataFormatLexer.IDENTIFIER = 5;



