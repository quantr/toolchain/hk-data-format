// Generated from HKDataFormatParser.g4 by ANTLR 4.9.1
// jshint ignore: start
import antlr4 from 'antlr4';
import HKDataFormatParserListener from './HKDataFormatParserListener.js';

const serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786",
    "\u5964\u0003\u0006\u001d\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004",
    "\u0004\t\u0004\u0003\u0002\u0003\u0002\u0003\u0002\u0003\u0003\u0007",
    "\u0003\r\n\u0003\f\u0003\u000e\u0003\u0010\u000b\u0003\u0003\u0004\u0005",
    "\u0004\u0013\n\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0003\u0004",
    "\u0005\u0004\u0019\n\u0004\u0003\u0004\u0003\u0004\u0003\u0004\u0002",
    "\u0002\u0005\u0002\u0004\u0006\u0002\u0002\u0002\u001c\u0002\b\u0003",
    "\u0002\u0002\u0002\u0004\u000e\u0003\u0002\u0002\u0002\u0006\u0012\u0003",
    "\u0002\u0002\u0002\b\t\u0005\u0004\u0003\u0002\t\n\u0007\u0002\u0002",
    "\u0003\n\u0003\u0003\u0002\u0002\u0002\u000b\r\u0005\u0006\u0004\u0002",
    "\f\u000b\u0003\u0002\u0002\u0002\r\u0010\u0003\u0002\u0002\u0002\u000e",
    "\f\u0003\u0002\u0002\u0002\u000e\u000f\u0003\u0002\u0002\u0002\u000f",
    "\u0005\u0003\u0002\u0002\u0002\u0010\u000e\u0003\u0002\u0002\u0002\u0011",
    "\u0013\u0007\u0003\u0002\u0002\u0012\u0011\u0003\u0002\u0002\u0002\u0012",
    "\u0013\u0003\u0002\u0002\u0002\u0013\u0014\u0003\u0002\u0002\u0002\u0014",
    "\u0015\u0007\u0004\u0002\u0002\u0015\u0018\u0007\u0005\u0002\u0002\u0016",
    "\u0017\u0007\u0006\u0002\u0002\u0017\u0019\u0007\u0005\u0002\u0002\u0018",
    "\u0016\u0003\u0002\u0002\u0002\u0018\u0019\u0003\u0002\u0002\u0002\u0019",
    "\u001a\u0003\u0002\u0002\u0002\u001a\u001b\b\u0004\u0001\u0002\u001b",
    "\u0007\u0003\u0002\u0002\u0002\u0005\u000e\u0012\u0018"].join("");


const atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

const decisionsToDFA = atn.decisionToState.map( (ds, index) => new antlr4.dfa.DFA(ds, index) );

const sharedContextCache = new antlr4.PredictionContextCache();

export default class HKDataFormatParserParser extends antlr4.Parser {

    static grammarFileName = "HKDataFormatParser.g4";
    static literalNames = [  ];
    static symbolicNames = [ null, "WS", "DASH", "IDENTIFIER", "COLON" ];
    static ruleNames = [ "hk", "lines", "line" ];

    constructor(input) {
        super(input);
        this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
        this.ruleNames = HKDataFormatParserParser.ruleNames;
        this.literalNames = HKDataFormatParserParser.literalNames;
        this.symbolicNames = HKDataFormatParserParser.symbolicNames;

            public void printWhitespaceBetweenRules(Token start) {
                int index = start.getTokenIndex() - 1;

                while(index >= 0) {
                    Token token = input.get(index);
                    if(token.getChannel() != Token.HIDDEN_CHANNEL) break;
                    System.out.print(token.getText());
                    index--;
                }
            }

    }

    get atn() {
        return atn;
    }



	hk() {
	    let localctx = new HkContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 0, HKDataFormatParserParser.RULE_hk);
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 6;
	        this.lines();
	        this.state = 7;
	        this.match(HKDataFormatParserParser.EOF);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	lines() {
	    let localctx = new LinesContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 2, HKDataFormatParserParser.RULE_lines);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 12;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        while(_la===HKDataFormatParserParser.WS || _la===HKDataFormatParserParser.DASH) {
	            this.state = 9;
	            this.line();
	            this.state = 14;
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	line() {
	    let localctx = new LineContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 4, HKDataFormatParserParser.RULE_line);
	    var _la = 0; // Token type
	    try {
	        localctx = new HandleLineContext(this, localctx);
	        this.enterOuterAlt(localctx, 1);
	        this.state = 16;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        if(_la===HKDataFormatParserParser.WS) {
	            this.state = 15;
	            this.match(HKDataFormatParserParser.WS);
	        }

	        this.state = 18;
	        this.match(HKDataFormatParserParser.DASH);
	        this.state = 19;
	        localctx.name = this.match(HKDataFormatParserParser.IDENTIFIER);
	        this.state = 22;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        if(_la===HKDataFormatParserParser.COLON) {
	            this.state = 20;
	            this.match(HKDataFormatParserParser.COLON);
	            this.state = 21;
	            localctx.attribute = this.match(HKDataFormatParserParser.IDENTIFIER);
	        }

	        printWhitespaceBetweenRules(localctx.start);System.out.println(this._input.getText(new antlr4.Interval(localctx.start, this._input.LT(-1))));
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}


}

HKDataFormatParserParser.EOF = antlr4.Token.EOF;
HKDataFormatParserParser.WS = 1;
HKDataFormatParserParser.DASH = 2;
HKDataFormatParserParser.IDENTIFIER = 3;
HKDataFormatParserParser.COLON = 4;

HKDataFormatParserParser.RULE_hk = 0;
HKDataFormatParserParser.RULE_lines = 1;
HKDataFormatParserParser.RULE_line = 2;

class HkContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParserParser.RULE_hk;
    }

	lines() {
	    return this.getTypedRuleContext(LinesContext,0);
	};

	EOF() {
	    return this.getToken(HKDataFormatParserParser.EOF, 0);
	};

	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterHk(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitHk(this);
		}
	}


}



class LinesContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParserParser.RULE_lines;
    }

	line = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(LineContext);
	    } else {
	        return this.getTypedRuleContext(LineContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterLines(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitLines(this);
		}
	}


}



class LineContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParserParser.RULE_line;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class HandleLineContext extends LineContext {

    constructor(parser, ctx) {
        super(parser);
        this.name = null; // Token;
        this.attribute = null; // Token;
        super.copyFrom(ctx);
    }

	DASH() {
	    return this.getToken(HKDataFormatParserParser.DASH, 0);
	};

	IDENTIFIER = function(i) {
		if(i===undefined) {
			i = null;
		}
	    if(i===null) {
	        return this.getTokens(HKDataFormatParserParser.IDENTIFIER);
	    } else {
	        return this.getToken(HKDataFormatParserParser.IDENTIFIER, i);
	    }
	};


	WS() {
	    return this.getToken(HKDataFormatParserParser.WS, 0);
	};

	COLON() {
	    return this.getToken(HKDataFormatParserParser.COLON, 0);
	};

	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterHandleLine(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitHandleLine(this);
		}
	}


}

HKDataFormatParserParser.HandleLineContext = HandleLineContext;


HKDataFormatParserParser.HkContext = HkContext; 
HKDataFormatParserParser.LinesContext = LinesContext; 
HKDataFormatParserParser.LineContext = LineContext; 
