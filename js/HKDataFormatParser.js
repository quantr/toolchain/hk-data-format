// Generated from HKDataFormatParser.g4 by ANTLR 4.9.2
// jshint ignore: start
import antlr4 from 'antlr4';
import HKDataFormatParserListener from './HKDataFormatParserListener.js';

const serializedATN = ["\u0003\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786",
    "\u5964\u0003\u0007*\u0004\u0002\t\u0002\u0004\u0003\t\u0003\u0004\u0004",
    "\t\u0004\u0004\u0005\t\u0005\u0003\u0002\u0003\u0002\u0003\u0002\u0003",
    "\u0003\u0007\u0003\u000f\n\u0003\f\u0003\u000e\u0003\u0012\u000b\u0003",
    "\u0003\u0004\u0005\u0004\u0015\n\u0004\u0003\u0004\u0003\u0004\u0003",
    "\u0004\u0003\u0004\u0005\u0004\u001b\n\u0004\u0003\u0004\u0006\u0004",
    "\u001e\n\u0004\r\u0004\u000e\u0004\u001f\u0003\u0005\u0003\u0005\u0003",
    "\u0005\u0007\u0005%\n\u0005\f\u0005\u000e\u0005(\u000b\u0005\u0003\u0005",
    "\u0002\u0002\u0006\u0002\u0004\u0006\b\u0002\u0002\u0002*\u0002\n\u0003",
    "\u0002\u0002\u0002\u0004\u0010\u0003\u0002\u0002\u0002\u0006\u0014\u0003",
    "\u0002\u0002\u0002\b!\u0003\u0002\u0002\u0002\n\u000b\u0005\u0004\u0003",
    "\u0002\u000b\f\u0007\u0002\u0002\u0003\f\u0003\u0003\u0002\u0002\u0002",
    "\r\u000f\u0005\u0006\u0004\u0002\u000e\r\u0003\u0002\u0002\u0002\u000f",
    "\u0012\u0003\u0002\u0002\u0002\u0010\u000e\u0003\u0002\u0002\u0002\u0010",
    "\u0011\u0003\u0002\u0002\u0002\u0011\u0005\u0003\u0002\u0002\u0002\u0012",
    "\u0010\u0003\u0002\u0002\u0002\u0013\u0015\u0007\u0003\u0002\u0002\u0014",
    "\u0013\u0003\u0002\u0002\u0002\u0014\u0015\u0003\u0002\u0002\u0002\u0015",
    "\u0016\u0003\u0002\u0002\u0002\u0016\u0017\u0007\u0005\u0002\u0002\u0017",
    "\u001a\u0005\b\u0005\u0002\u0018\u0019\u0007\u0006\u0002\u0002\u0019",
    "\u001b\u0005\b\u0005\u0002\u001a\u0018\u0003\u0002\u0002\u0002\u001a",
    "\u001b\u0003\u0002\u0002\u0002\u001b\u001d\u0003\u0002\u0002\u0002\u001c",
    "\u001e\u0007\u0004\u0002\u0002\u001d\u001c\u0003\u0002\u0002\u0002\u001e",
    "\u001f\u0003\u0002\u0002\u0002\u001f\u001d\u0003\u0002\u0002\u0002\u001f",
    " \u0003\u0002\u0002\u0002 \u0007\u0003\u0002\u0002\u0002!&\u0007\u0007",
    "\u0002\u0002\"#\u0007\u0005\u0002\u0002#%\u0007\u0007\u0002\u0002$\"",
    "\u0003\u0002\u0002\u0002%(\u0003\u0002\u0002\u0002&$\u0003\u0002\u0002",
    "\u0002&\'\u0003\u0002\u0002\u0002\'\t\u0003\u0002\u0002\u0002(&\u0003",
    "\u0002\u0002\u0002\u0007\u0010\u0014\u001a\u001f&"].join("");


const atn = new antlr4.atn.ATNDeserializer().deserialize(serializedATN);

const decisionsToDFA = atn.decisionToState.map( (ds, index) => new antlr4.dfa.DFA(ds, index) );

const sharedContextCache = new antlr4.PredictionContextCache();

export default class HKDataFormatParser extends antlr4.Parser {

    static grammarFileName = "HKDataFormatParser.g4";
    static literalNames = [ null, null, null, "'-'", "'='" ];
    static symbolicNames = [ null, "WS", "NL", "DASH", "EQUAL", "IDENTIFIER" ];
    static ruleNames = [ "hk", "lines", "line", "identifier" ];

    constructor(input) {
        super(input);
        this._interp = new antlr4.atn.ParserATNSimulator(this, atn, decisionsToDFA, sharedContextCache);
        this.ruleNames = HKDataFormatParser.ruleNames;
        this.literalNames = HKDataFormatParser.literalNames;
        this.symbolicNames = HKDataFormatParser.symbolicNames;
    }

    get atn() {
        return atn;
    }



	hk() {
	    let localctx = new HkContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 0, HKDataFormatParser.RULE_hk);
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 8;
	        this.lines();
	        this.state = 9;
	        this.match(HKDataFormatParser.EOF);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	lines() {
	    let localctx = new LinesContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 2, HKDataFormatParser.RULE_lines);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 14;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        while(_la===HKDataFormatParser.WS || _la===HKDataFormatParser.DASH) {
	            this.state = 11;
	            this.line();
	            this.state = 16;
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	line() {
	    let localctx = new LineContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 4, HKDataFormatParser.RULE_line);
	    var _la = 0; // Token type
	    try {
	        localctx = new HandleLineContext(this, localctx);
	        this.enterOuterAlt(localctx, 1);
	        this.state = 18;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        if(_la===HKDataFormatParser.WS) {
	            this.state = 17;
	            this.match(HKDataFormatParser.WS);
	        }

	        this.state = 20;
	        this.match(HKDataFormatParser.DASH);
	        this.state = 21;
	        localctx.name = this.identifier();
	        this.state = 24;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        if(_la===HKDataFormatParser.EQUAL) {
	            this.state = 22;
	            this.match(HKDataFormatParser.EQUAL);
	            this.state = 23;
	            localctx.attribute = this.identifier();
	        }

	        this.state = 27; 
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        do {
	            this.state = 26;
	            this.match(HKDataFormatParser.NL);
	            this.state = 29; 
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        } while(_la===HKDataFormatParser.NL);
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}



	identifier() {
	    let localctx = new IdentifierContext(this, this._ctx, this.state);
	    this.enterRule(localctx, 6, HKDataFormatParser.RULE_identifier);
	    var _la = 0; // Token type
	    try {
	        this.enterOuterAlt(localctx, 1);
	        this.state = 31;
	        this.match(HKDataFormatParser.IDENTIFIER);
	        this.state = 36;
	        this._errHandler.sync(this);
	        _la = this._input.LA(1);
	        while(_la===HKDataFormatParser.DASH) {
	            this.state = 32;
	            this.match(HKDataFormatParser.DASH);
	            this.state = 33;
	            this.match(HKDataFormatParser.IDENTIFIER);
	            this.state = 38;
	            this._errHandler.sync(this);
	            _la = this._input.LA(1);
	        }
	    } catch (re) {
	    	if(re instanceof antlr4.error.RecognitionException) {
		        localctx.exception = re;
		        this._errHandler.reportError(this, re);
		        this._errHandler.recover(this, re);
		    } else {
		    	throw re;
		    }
	    } finally {
	        this.exitRule();
	    }
	    return localctx;
	}


}

HKDataFormatParser.EOF = antlr4.Token.EOF;
HKDataFormatParser.WS = 1;
HKDataFormatParser.NL = 2;
HKDataFormatParser.DASH = 3;
HKDataFormatParser.EQUAL = 4;
HKDataFormatParser.IDENTIFIER = 5;

HKDataFormatParser.RULE_hk = 0;
HKDataFormatParser.RULE_lines = 1;
HKDataFormatParser.RULE_line = 2;
HKDataFormatParser.RULE_identifier = 3;

class HkContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParser.RULE_hk;
    }

	lines() {
	    return this.getTypedRuleContext(LinesContext,0);
	};

	EOF() {
	    return this.getToken(HKDataFormatParser.EOF, 0);
	};

	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterHk(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitHk(this);
		}
	}


}



class LinesContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParser.RULE_lines;
    }

	line = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(LineContext);
	    } else {
	        return this.getTypedRuleContext(LineContext,i);
	    }
	};

	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterLines(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitLines(this);
		}
	}


}



class LineContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParser.RULE_line;
    }


	 
		copyFrom(ctx) {
			super.copyFrom(ctx);
		}

}


class HandleLineContext extends LineContext {

    constructor(parser, ctx) {
        super(parser);
        this.name = null; // IdentifierContext;
        this.attribute = null; // IdentifierContext;
        super.copyFrom(ctx);
    }

	DASH() {
	    return this.getToken(HKDataFormatParser.DASH, 0);
	};

	identifier = function(i) {
	    if(i===undefined) {
	        i = null;
	    }
	    if(i===null) {
	        return this.getTypedRuleContexts(IdentifierContext);
	    } else {
	        return this.getTypedRuleContext(IdentifierContext,i);
	    }
	};

	WS() {
	    return this.getToken(HKDataFormatParser.WS, 0);
	};

	EQUAL() {
	    return this.getToken(HKDataFormatParser.EQUAL, 0);
	};

	NL = function(i) {
		if(i===undefined) {
			i = null;
		}
	    if(i===null) {
	        return this.getTokens(HKDataFormatParser.NL);
	    } else {
	        return this.getToken(HKDataFormatParser.NL, i);
	    }
	};


	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterHandleLine(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitHandleLine(this);
		}
	}


}

HKDataFormatParser.HandleLineContext = HandleLineContext;

class IdentifierContext extends antlr4.ParserRuleContext {

    constructor(parser, parent, invokingState) {
        if(parent===undefined) {
            parent = null;
        }
        if(invokingState===undefined || invokingState===null) {
            invokingState = -1;
        }
        super(parent, invokingState);
        this.parser = parser;
        this.ruleIndex = HKDataFormatParser.RULE_identifier;
    }

	IDENTIFIER = function(i) {
		if(i===undefined) {
			i = null;
		}
	    if(i===null) {
	        return this.getTokens(HKDataFormatParser.IDENTIFIER);
	    } else {
	        return this.getToken(HKDataFormatParser.IDENTIFIER, i);
	    }
	};


	DASH = function(i) {
		if(i===undefined) {
			i = null;
		}
	    if(i===null) {
	        return this.getTokens(HKDataFormatParser.DASH);
	    } else {
	        return this.getToken(HKDataFormatParser.DASH, i);
	    }
	};


	enterRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.enterIdentifier(this);
		}
	}

	exitRule(listener) {
	    if(listener instanceof HKDataFormatParserListener ) {
	        listener.exitIdentifier(this);
		}
	}


}




HKDataFormatParser.HkContext = HkContext; 
HKDataFormatParser.LinesContext = LinesContext; 
HKDataFormatParser.LineContext = LineContext; 
HKDataFormatParser.IdentifierContext = IdentifierContext; 
