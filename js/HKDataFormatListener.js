// Generated from HKDataFormat.g4 by ANTLR 4.9.1
// jshint ignore: start
import antlr4 from 'antlr4';

// This class defines a complete listener for a parse tree produced by HKDataFormatParser.
export default class HKDataFormatListener extends antlr4.tree.ParseTreeListener {

	// Enter a parse tree produced by HKDataFormatParser#prog.
	enterProg(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#prog.
	exitProg(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#stat.
	enterStat(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#stat.
	exitStat(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#assignment.
	enterAssignment(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#assignment.
	exitAssignment(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#myid.
	enterMyid(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#myid.
	exitMyid(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#myexpr.
	enterMyexpr(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#myexpr.
	exitMyexpr(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#myint.
	enterMyint(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#myint.
	exitMyint(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#add_sub.
	enterAdd_sub(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#add_sub.
	exitAdd_sub(ctx) {
	}


	// Enter a parse tree produced by HKDataFormatParser#multi_div.
	enterMulti_div(ctx) {
	}

	// Exit a parse tree produced by HKDataFormatParser#multi_div.
	exitMulti_div(ctx) {
	}



}