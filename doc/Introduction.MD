# Introduction

A new file format

![](https://peter.quantr.hk/wp-content/uploads/2021/02/img_20210215_1720548272647812469271190-scaled.jpg)

## Technology stack

We use the following things for development

1. Java
2. Antlr
3. Netbeans (Not a must) but we have build our own antlr plugin for netbeans, easy to trace antlr grammar bug
4. Maven

## Example

### Write to .hk

```
import hk.quantr.hkdataformat.HKDataFormat;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestWrite {

	@Test
	public void test() throws FileNotFoundException, IOException {
		Node header = new Node("HK");
		header.add(new Node("magic", "HK"));
		header.add(new Node("name", "kernel"));
		header.add(new Node("developer", "Peter Cheung <peter@quantr.hk>"));
		Node section = header.add(new Node("section"));

		Node code = section.add(new Node("code"));
		code.add(new Node("file_size", 0x10000));
		code.add(new Node("virt_addr", 0x10000));
		code.add(new Node("virt_size", 0x10000));
		code.add(new Node("flag", "rwx"));

		Node data = section.add(new Node("data"));
		data.add(new Node("file_size", 0x10000));
		data.add(new Node("virt_addr", 0x10000));
		data.add(new Node("virt_size", 0x10000));
		data.add(new Node("flag", "rw"));
		
		System.out.println(data.getPath());

		FileOutputStream fo = new FileOutputStream("1.hk");
		HKDataFormat.write(fo, header);
		fo.close();
	}
}
```

### Read from .hk

```
import hk.quantr.hkdataformat.antlr.HKDataFormatLexer;
import hk.quantr.hkdataformat.antlr.HKDataFormatParser;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestParser {

	@Test
	public void test() throws FileNotFoundException, IOException {
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromStream(new FileInputStream("1.hk")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();

		Node root = listener.root;
		root.print(0);
	}
}
```
