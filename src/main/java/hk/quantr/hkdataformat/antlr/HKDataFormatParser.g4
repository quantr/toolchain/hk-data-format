parser grammar HKDataFormatParser;
options { tokenVocab=HKDataFormatLexer; }

hk			:	lines EOF
			;
			
lines		:	line*
			;

line		:	WS? DASH name=identifier (EQUAL attribute=identifier)? NL+	#handleLine
			;

identifier	:	IDENTIFIER (DASH IDENTIFIER)*
			;
