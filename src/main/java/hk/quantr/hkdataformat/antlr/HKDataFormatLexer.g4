lexer grammar HKDataFormatLexer;

WS			:	(' '|'\t')+ -> channel(1);
NL			:	'\r'? '\n';

DASH		:	'-';
//COLON		:	':';
EQUAL		:	'=';
IDENTIFIER	:	[a-zA-Z0-9 ~_<>#:\\.@()/]+;

