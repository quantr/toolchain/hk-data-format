package hk.quantr.hkdataformat;

import hk.quantr.hkdataformat.annotation.HKSerializable;
import hk.quantr.hkdataformat.antlr.HKDataFormatLexer;
import hk.quantr.hkdataformat.antlr.HKDataFormatParser;
import hk.quantr.hkdataformat.datastructure.Node;
import hk.quantr.javalib.CommonLib;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class HKDataFormat {

	private static final Logger logger = Logger.getLogger(HKDataFormat.class.getName());

	public static void write(OutputStream os, Node node) throws IOException {
		write(os, node, 0);
	}

	private static void write(OutputStream os, Node node, int indent) throws IOException {
		writeString(os, " ".repeat(indent) + "-" + node.name);
		if (node.value != null) {
			if (node.value instanceof byte[]) {
				byte[] bytes = (byte[]) node.value;
				writeString(os, ":" + CommonLib.arrayToHexString(bytes));
			} else if (node.value instanceof Integer) {
				int i = (int) node.value;
				writeString(os, ":0x" + Integer.toHexString(i));
			} else if (node.value instanceof File) {
				File file = (File) node.value;
				writeString(os, ":@(" + file.getAbsolutePath() + ")");
			} else if (node.value instanceof URL) {
				URL url = (URL) node.value;
				writeString(os, ":@(" + url + ")");
			} else {
				writeString(os, ":" + node.value.toString());
			}
		}
		writeString(os, "\n");
		for (Node child : node.children) {
			write(os, child, indent + 1);
		}
	}

	private static void writeString(OutputStream os, String str) throws IOException {
		os.write(str.getBytes());
	}

	public static void main(String args[]) throws FileNotFoundException, IOException, IOException, CloneNotSupportedException {
		CommandLineParser parser1 = new DefaultParser();
		Options options = new Options();
		try {
			options.addOption(Option.builder("c")
					.required(false)
					.argName("command")
					.hasArg()
					.numberOfArgs(1)
					.build());

			CommandLine cmd = parser1.parse(options, args);
			if (args.length == 0 || Arrays.asList(args).contains("-h") || Arrays.asList(args).contains("--help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("java -jar assembler-xx.jar [OPTION] <input file>", options);
				return;
			}
			if (!cmd.hasOption("c")) {
				System.out.println("no command");
				System.exit(1);
			} else {
				if (cmd.getOptionValue("c").equals("print")) {
					System.out.println(get(new File(cmd.getArgs()[0]), cmd.getArgs()[1]));
				} else if (cmd.getOptionValue("c").equals("copy")) {
					String from = cmd.getArgs()[1];
					String to = cmd.getArgs()[2];
					copy(new File(cmd.getArgs()[0]), from, to);
				} else if (cmd.getOptionValue("c").equals("move")) {
					String from = cmd.getArgs()[1];
					String to = cmd.getArgs()[2];
					move(new File(cmd.getArgs()[0]), from, to);
				} else if (cmd.getOptionValue("c").equals("delete")) {
					String from = cmd.getArgs()[1];
					delete(new File(cmd.getArgs()[0]), from);
				}
			}
		} catch (ParseException ex) {
			logger.log(Level.SEVERE, null, ex);
			System.exit(1);
		}
	}

	public static String get(File file, String query) {
		Node node = HKDataFormatLower.getNode(file, query);
		if (node == null || node.value == null) {
			return null;
		}
		return node.value.toString();
	}

	public static void copy(File file, String from, String to) throws FileNotFoundException, IOException {
		Node root = getRoot(file);
		Node nodeFrom = HKDataFormatLower.getNode(root, from);
		Node nodeTo = HKDataFormatLower.getNode(root, to);
		nodeTo.add(nodeFrom);
		String s = root.toString(0);
		//IOUtils.write(s, new FileOutputStream(file), "utf-8");
		System.out.println(s);
	}

	public static void move(File file, String from, String to) throws FileNotFoundException, IOException, CloneNotSupportedException {
		Node root = getRoot(file);
		Node nodeFrom = HKDataFormatLower.getNode(root, from);
		Node nodeTo = HKDataFormatLower.getNode(root, to);
		nodeTo.add((Node) nodeFrom.clone());
		HKDataFormatLower.remove(root, nodeFrom);
		String s = root.toString(0);
		IOUtils.write(s, new FileOutputStream(file), "utf-8");
		System.out.println(s);
	}

	public static void delete(File file, String from) throws FileNotFoundException, IOException, CloneNotSupportedException {
		Node root = getRoot(file);
		Node nodeFrom = HKDataFormatLower.getNode(root, from);
		HKDataFormatLower.remove(root, nodeFrom);
		String s = root.toString(0);
		IOUtils.write(s, new FileOutputStream(file), "utf-8");
		System.out.println(s);
	}

	public static Node getRoot(String content) throws FileNotFoundException, IOException {
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();

		return listener.root;
	}

	public static Node getRoot(File file) throws FileNotFoundException, IOException {
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromStream(new FileInputStream(file)));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();

		return listener.root;
	}

	public static Node toHK(Object object) throws IllegalArgumentException, IllegalAccessException {
		Class c = object.getClass();
		if (!c.isAnnotationPresent(HKSerializable.class)) {
			return null;
		}
		Field fields[] = c.getDeclaredFields();
		Node node = new Node("name", object.getClass().getName());
		for (int x = 0; x < fields.length; x++) {
			int modifiers = fields[x].getModifiers();
			if (Modifier.isPublic(modifiers)) {
				fields[x].setAccessible(true);
				node.children.add(new Node(fields[x].getName(), String.valueOf(fields[x].getInt(object))));
//				System.out.println(fields[x].getName() + " = " + fields[x].getInt(object));
			}
		}
		return node;
	}

	public static <T> T fromHK(String content, Class<T> c) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException, NoSuchFieldException {
		T object = (T) c.newInstance();
		Node root = getRoot(content);
		System.out.println(root);
		for (Node child : root.children) {
//			System.out.println(">>> " + child.name + " ==== " + child.value);
			Field field = object.getClass().getDeclaredField(child.name);
//			System.out.println(field.getGenericType());
			if (field.getGenericType() == Integer.TYPE) {
				field.setInt(object, (int) child.value);
			}
		}
		return object;
	}

}
