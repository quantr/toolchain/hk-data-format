package hk.quantr.hkdataformat;

import hk.quantr.hkdataformat.antlr.HKDataFormatLexer;
import hk.quantr.hkdataformat.antlr.HKDataFormatParser;
import hk.quantr.hkdataformat.antlr.HKDataFormatParserBaseListener;
import hk.quantr.hkdataformat.datastructure.Node;
import java.util.HashMap;
import java.util.List;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStreamRewriter;
import org.apache.commons.lang3.math.NumberUtils;

/**
 *
 * @author Sammy
 */
public class MyListener extends HKDataFormatParserBaseListener {

	public Node root;
	HKDataFormatLexer lexer;
	BufferedTokenStream tokens;
	TokenStreamRewriter rewriter;

	HashMap<Integer, Node> maps = new HashMap<Integer, Node>();

	public MyListener(HKDataFormatLexer lexer, BufferedTokenStream tokens) {
		this.lexer = lexer;
		this.tokens = tokens;
	}

	@Override
	public void exitHandleLine(HKDataFormatParser.HandleLineContext ctx) {
		List<Token> t = tokens.getHiddenTokensToLeft(ctx.start.getTokenIndex());
		int noOfSpace = 0;
		if (t != null) {
			for (Token tt : t) {
				if (lexer.getVocabulary().getSymbolicName(tt.getType()).equals("WS")) {
					noOfSpace = tt.getText().length();
					break;
				}
			}
		}
		String name = ctx.name.getText();
		String str = ctx.attribute == null ? null : ctx.attribute.getText();
		Object value;
		if (NumberUtils.isCreatable(str)) {
			value = NumberUtils.createInteger(str);
		} else {
			value = str;
		}
		if (noOfSpace == 0) {
			if (root == null) {
				root = new Node(name, value);
				maps.put(noOfSpace, root);
			} else {
				System.err.println("Multiple root node exists");
			}
		} else {
			Node parentNode = maps.get(noOfSpace - 1);
			Node child = new Node(name, value);
			parentNode.add(child);
			maps.put(noOfSpace, child);
		}
	}
}
