/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.hkdataformat.datastructure;

import java.util.ArrayList;
import java.util.Collections;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Node implements Cloneable {

	public String name;
	public Object value;
	public Node parent;
	public ArrayList<Node> children = new ArrayList<>();

	public Node(String name, Object value) {
		this.name = name;
		this.value = value;
	}

	public Node(String name) {
		this(name, null);
	}

	public Node add(Node child) {
		child.parent = this;
		children.add(child);
		return child;
	}

	public void print(int level) {
		System.out.println(" ".repeat(level) + "-" + this);
		for (Node child : children) {
			child.print(level + 1);
		}
	}

	public String toString(int level) {
		String s = " ".repeat(level) + "-" + toString() + "\n";
		for (Node child : children) {
			s += child.toString(level + 1);
		}
		return s;
	}

	@Override
	public String toString() {
		String str = name + (value != null ? "=" + value : "");
//		for (Node c : children) {
//			str += " -" + c;
//		}
		return str;
	}

	public String getPath() {
		ArrayList<Node> nodes = new ArrayList<Node>();
		nodes.add(this);
		Node root = parent;
		nodes.add(root);
		while (root.parent != null) {
			root = root.parent;
			nodes.add(root);
		}
		Collections.reverse(nodes);
		String path = StringUtils.join(nodes, "/");
		return path;
	}

	public Node get(String name) {
		for (Node child : children) {
			if (child.name.equals(name)) {
				return child;
			}
		}
		return null;
	}

	public String getValue(String name) {
		for (Node child : children) {
			if (child.name.equals(name)) {
				return (String) child.value;
			}
		}
		return null;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
