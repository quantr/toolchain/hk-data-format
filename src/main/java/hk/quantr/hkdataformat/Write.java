//package hk.quantr.hkdataformat;
//
//
//
//import hk.quantr.hkdataformat.HKDataFormat;
//import hk.quantr.hkdataformat.datastructure.Node;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.net.URL;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class Write {
//
//	
//	public void test() throws FileNotFoundException, IOException {
//		Node header = new Node("HK");
//		header.add(new Node("magic", "HK"));
//		header.add(new Node("name", "kernel"));
//		header.add(new Node("developer", "Peter Cheung <peter@quantr.hk>"));
//		Node section = header.add(new Node("section"));
//
//		Node code = section.add(new Node("code"));
//		code.add(new Node("file_size", 0x10000));
//		code.add(new Node("virt_addr", 0x10000));
//		code.add(new Node("virt_size", 0x10000));
//		code.add(new Node("flag", "rwx"));
//		
//		Node code2 = section.add(new Node("code2"));
//		code2.add(new Node("file_size", 0x00000));
//
//		Node data = section.add(new Node("data"));
//		data.add(new Node("file_size", 0x10000));
//		data.add(new Node("virt_addr", 0x10000));
//		data.add(new Node("virt_size", new File("C:\\Users\\Sammy\\Downloads\\Elearning Result.xlsx")));
//		data.add(new Node("flag", new URL("https://www.google.com")));
//		
//		System.out.println(data.getPath());
//
//		FileOutputStream fo = new FileOutputStream("1.hk");
//		HKDataFormat.write(fo, header);
//		fo.close();
//	}
//}
