/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.hkdataformat;

import hk.quantr.hkdataformat.antlr.HKDataFormatLexer;
import hk.quantr.hkdataformat.antlr.HKDataFormatParser;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class HKDataFormatLower {

	public static ArrayList<Node> getNodes(ArrayList<Node> nodes, Node node, String[] arr) {
		if (arr.length == 0) {
			nodes.add(node);
			return nodes;
		} else {
			if (arr.length > 0 && node.name.equals(arr[0])) {
				String[] temp = Arrays.copyOfRange(arr, 1, arr.length);
				ArrayList<Node> tempNodes = getNodes(nodes, node, temp);
			} else {
				for (Node child : node.children) {
					if (arr.length > 0 && child.name.equals(arr[0])) {
						String[] temp = Arrays.copyOfRange(arr, 1, arr.length);
						ArrayList<Node> tempNodes = getNodes(nodes, child, temp);
					}
				}
			}
		}
		return nodes;
	}

	public static ArrayList<Node> getNodes(File filepath, String query) throws IOException {
		String[] strs = query.split("/");
		strs = Arrays.copyOfRange(strs, 1, strs.length);
		Node root = getRoot(filepath);
		ArrayList<Node> nodes = getNodes(new ArrayList<Node>(), root, strs);
		return nodes;
	}

	private static Node getRoot(File file) throws FileNotFoundException, IOException {
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromStream(new FileInputStream(file)));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();
		Node root = listener.root;
		return root;
	}

	private static Node getRoot(String content) throws FileNotFoundException, IOException {
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromString(content));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();
		Node root = listener.root;
		return root;
	}

	public static Node getNode(Node node, String path) {
		String[] temp = path.split("/");
		temp = Arrays.copyOfRange(temp, 1, temp.length);
		return getNode(node, temp);
	}

	public static Node getNode(Node node, String[] arr) {
		if (arr.length == 0) {
			return node;
		} else {
			if (node.name.equals(arr[0])) {
				arr = Arrays.copyOfRange(arr, 1, arr.length);
				Node tempNode = getNode(node, arr);
				return tempNode;
			} else {
				for (Node child : node.children) {
					if (child.name.equals(arr[0])) {
						arr = Arrays.copyOfRange(arr, 1, arr.length);
						Node tempNode = getNode(child, arr);
						return tempNode;
					}
				}
			}
		}
		return null;
	}

	public static Node getNode(String content, String query) {
		try {
			String[] strs = query.split("/");
			strs = Arrays.copyOfRange(strs, 1, strs.length);
			Node root = getRoot(content);
			Node node = getNode(root, strs);
			return node;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static Node getNode(File file, String query) {
		try {
			String[] strs = query.split("/");
			strs = Arrays.copyOfRange(strs, 1, strs.length);
			Node root = getRoot(file);
			Node node = getNode(root, strs);
			return node;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}

	public static void remove(Node node, Node n) {
		node.children.remove(n);
		for (Node c : node.children) {
			remove(c, n);
		}
	}

}
