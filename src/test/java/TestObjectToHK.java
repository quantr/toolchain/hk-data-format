
import hk.quantr.hkdataformat.HKDataFormat;
import hk.quantr.hkdataformat.datastructure.Node;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestObjectToHK {

	@Test
	public void test() throws IllegalArgumentException, IllegalAccessException {
		Test1 test1 = new Test1();
//		test1.x = 12;
		test1.y = 34;
		Node node = HKDataFormat.toHK(test1);
		System.out.println(node.toString(0));
		
		System.out.println("-------------");

		try {
			Test1 test2 = HKDataFormat.fromHK(node.toString(0), Test1.class);
			System.out.println(test2);
		} catch (Exception ex) {
			Logger.getLogger(TestObjectToHK.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
