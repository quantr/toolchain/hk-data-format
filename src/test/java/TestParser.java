
import hk.quantr.hkdataformat.MyListener;
import hk.quantr.hkdataformat.antlr.HKDataFormatLexer;
import hk.quantr.hkdataformat.antlr.HKDataFormatParser;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestParser {

	@Test
	public void test() throws FileNotFoundException, IOException {
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromStream(new FileInputStream(TestParser.class.getResource("shatin.hk").getPath())));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();

		Node root = listener.root;
		root.print(0);
	}
}
