
import hk.quantr.hkdataformat.MyListener;
import hk.quantr.hkdataformat.antlr.HKDataFormatLexer;
import hk.quantr.hkdataformat.antlr.HKDataFormatParser;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/**
 *
 * @author Sammy
 */
public class TestQuery {

	@Test
	public void test() throws FileNotFoundException, IOException {
		String query = "/HK/section/code/virt_addr";
		String[] strs = query.split("/");
		strs = Arrays.copyOfRange(strs, 1, strs.length);
		HKDataFormatLexer lexer = new HKDataFormatLexer(CharStreams.fromStream(new FileInputStream("1.hk")));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		HKDataFormatParser parser = new HKDataFormatParser(tokenStream);
		MyListener listener = new MyListener(lexer, tokenStream);
		parser.addParseListener(listener);
		parser.hk();

		Node root = listener.root;

		Node node = getNode(root, strs);
		System.out.println("=================");
		System.out.println(node.name);
		System.out.println("=================");
	}

	public static void main(String args[]) throws FileNotFoundException, IOException, InterruptedException {
		System.out.println("Running program");
		new TestQuery();
	}

	public Node getNode(Node node, String arr[]) {
		if (arr.length == 0) {
			return node;
		} else {
			if (node.name.equals(arr[0])) {
				arr = Arrays.copyOfRange(arr, 1, arr.length);
				Node tempNode = getNode(node, arr);
				return tempNode;
			} else {
				for (Node child : node.children) {
//					if(arr.length==1 && child.name.split(":")[0].equals(arr[0])){
//						arr = Arrays.copyOfRange(arr, 1, arr.length);
//						Node tempNode = getNode(child, arr);
//						return tempNode;
//					}else if (child.name.equals(arr[0])) {
//						arr = Arrays.copyOfRange(arr, 1, arr.length);
//						Node tempNode = getNode(child, arr);
//						return tempNode;
//					}

					if (child.name.split(":")[0].equals(arr[0])) {
						arr = Arrays.copyOfRange(arr, 1, arr.length);
						Node tempNode = getNode(child, arr);
						return tempNode;
					}
				}
			}
		}
		return null;
	}
}
