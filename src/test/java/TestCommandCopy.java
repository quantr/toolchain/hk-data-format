
import hk.quantr.hkdataformat.HKDataFormat;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.junit.Test;

/*
 * Copyright 2021 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestCommandCopy {

	@Test
	public void test() throws IOException, FileNotFoundException, InterruptedException, CloneNotSupportedException {
		HKDataFormat.main((TestCommand1.class.getResource("1.hk").getPath() + " -c copy /MYDATA/header/section1 /MYDATA/header").split(" "));
	}
}
