
import hk.quantr.hkdataformat.HKDataFormat;
import hk.quantr.hkdataformat.HKDataFormatLower;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;

public class TestAPI {

	@Test
	public void test() throws FileNotFoundException, IOException {
		String name = HKDataFormat.get(new File(TestAPI.class.getResource("shatin.hk").getPath()), "/shatin/hosts/host");
		System.out.println(name);
		System.out.println("-".repeat(20));

		Node node = HKDataFormatLower.getNode(new File(TestAPI.class.getResource("shatin.hk").getPath()), "//path");
		System.out.println(node);
		System.out.println("-".repeat(20));

		ArrayList<Node> nodes = HKDataFormatLower.getNodes(new File(TestAPI.class.getResource("shatin.hk").getPath()), "/hosts/name");
		for (Node n : nodes) {
			System.out.println(n);
			System.out.println("\tport=" + n.get("port").value);
		}
		System.out.println("-".repeat(20));
	}
}
