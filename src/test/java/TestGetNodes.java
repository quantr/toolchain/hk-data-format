
import hk.quantr.hkdataformat.HKDataFormatLower;
import hk.quantr.hkdataformat.datastructure.Node;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestGetNodes {

	@Test
	public void test() throws FileNotFoundException, IOException {
		ArrayList<Node> nodes = HKDataFormatLower.getNodes(new File("/Users/peter/workspace/shatin/test/shatin.hk"), "/shatin/hosts/host");
		for (Node host : nodes) {
			System.out.println(host.get("path").value);
		}
	}
}
