# HK Data Format

## Goal

A simple, human readable format to support our RISC-V toolchain

## Author

Peter <peter@quantr.hk>

## How to use this lib

By Maven:

```
<repositories>
    <repository>
        <id>quantr</id>
        <url>https://maven.quantr.hk/repo</url>
    </repository>
</repositories>
```

```
<dependency>
	<groupId>hk.quantr</groupId>
	<artifactId>HKDataFormat</artifactId>
</dependency>
```

## File extension

.hk

## Syntax

```
-MYDATA
 -header
  -section1
   -offset=0x100
   -size=0x200
   -name=@(/MYDATA/stringtable/data1)
  -section2
   -name=@(stringtable/data2)
 -program section1
  -section
   -data=@(bin1)
  -section
   -data=@(file://peter.bin)
  -section
   -data=@(https://www.quantr.foundation/peter.bin)
 -stringtable
  -data1=Peter
  -data2=Sammy
 -binary
  -bin1:base64(UEV0ZXI=)
  -bin2:array(0x12,0x34,0x45)
```

## Command set

java -jar hk-data-format.jar 1.hk -c print /MYDATA/header/section1/offset

java -jar hk-data-format.jar 1.hk -c print section1/offset

java -jar hk-data-format.jar 1.hk -c copy /MYDATA/header/section1 /MYDATA/header/section3

java -jar hk-data-format.jar 1.hk -c move /MYDATA/header/section1 /MYDATA/header/section3

java -jar hk-data-format.jar 1.hk -c rename /MYDATA/header/section1 section3

java -jar hk-data-format.jar 1.hk -c delete /MYDATA/header/section1

java -jar hk-data-format.jar 1.hk -c delete section1

## Specification

![](https://peter.quantr.hk/wp-content/uploads/2021/02/img_20210214_1325551403381845990063950-scaled.jpg)

## Our road

![](https://peter.quantr.hk/wp-content/uploads/2021/02/HK-Data-Format.drawio-diagram.png)

![](https://peter.quantr.hk/wp-content/uploads/2021/02/img_20210215_1720548272647812469271190-scaled.jpg)


## Grammar

```
grammar HK;

```
